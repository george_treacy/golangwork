package kyoolibs

import (
	"log"
	"net/url"
	"strings"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sqs"
	"encoding/json"
	"crypto/md5"
	"encoding/hex"
)

type S3Summary struct {
	Bucket 	string
	Key 	string
	Etag 	string
	PathMd5 string
}

/*
	For now, if HeadObject returns nil, that means that we want
	to bail asap and DeleteMessage but *not* push to new Queue.
 */
func HeadObject(svc *s3.S3, message *sqs.Message) *S3Summary {
	s3Summary := getSummaryFromEvent(message)

	/*
		If we don't get a proper message
		should we bomb out and retry or just
		log it and not send a delete message?
	 */
	if s3Summary == nil {
		return nil
	}

	// filter out any junk from OXS file systems...
	if strings.Contains(s3Summary.Key, ".DS_STORE") {
		log.Printf(s3Summary.Etag, "found ds_store")
		// still need to make sure the message gets
		// deleted so that we don't keep trying to process this.
		return nil
	}

	// The json version of the key has + replacing space.
	// Need to put back the spaces.
	fixedString, err := url.QueryUnescape(s3Summary.Key)
	if err != nil {
		log.Println("could not unescape url key", s3Summary.Key)
		return nil
	}

	params := &s3.HeadObjectInput{
		Bucket:                     &s3Summary.Bucket,
		Key:                       	&fixedString,
	}
	resp, err := svc.HeadObject(params)

	if err != nil {
		log.Println("ERROR HeadObject", params, err)
		return nil
	}

	// add the etag to the summary and return.
	s3Summary.Etag = strings.Trim(*resp.ETag, "\"");
	return s3Summary
}

func GetS3Object(svc *s3.S3, message *sqs.Message) *S3Summary {
	println("about to do json extraction from message")
	s3Summary := getSummaryFromEvent(message)
	println("done with json extraction from message")

	// filter out any junk from OXS file systems...
	if strings.Contains(s3Summary.Key, ".DS_STORE") {
		log.Printf(s3Summary.Etag, "found ds_store")
		return nil
	}

	// The json version of the key has + replacing space.
	// Need to put back the spaces.
	fixedString := strings.Replace(s3Summary.Key, "+", " ", -1)

	params := &s3.GetObjectInput{
		Bucket:                     &s3Summary.Bucket,
		Key:                       	&fixedString,
	}
	resp, err := svc.GetObject(params)

	if err != nil {
		log.Println(s3Summary.Etag, "GetS3Object", err.Error())
		return nil
	}

	// add the etag to the summary and return.
	s3Summary.Etag = *resp.ETag;
	log.Println(s3Summary.Etag, "Parsed Message to Summary", s3Summary)
	return s3Summary
}

func getSummaryFromEvent(message *sqs.Message) *S3Summary {
	myBody := Body{}
	err := json.Unmarshal([]byte(*message.Body), &myBody)
	if err != nil {
		log.Println("error unmarshalling json from message.")
		panic(err)
	}

	s3Summary := S3Summary{}
	if &myBody.Records != nil && len(myBody.Records) > 0 {
		bucket := myBody.Records[0].S3.Bucket.Name
		key := myBody.Records[0].S3.Object.Key
		s3Summary.Bucket = bucket
		s3Summary.Key = key
		s3Summary.PathMd5 = getMd5Hash(bucket, key)
		return &s3Summary
	} else {
		return nil
	}
}

func getMd5Hash(bucket string, key string) string {
	myPath := bucket + "/" + key
	hasher := md5.New()
	hasher.Write([]byte(myPath))
	return hex.EncodeToString(hasher.Sum(nil))
}





