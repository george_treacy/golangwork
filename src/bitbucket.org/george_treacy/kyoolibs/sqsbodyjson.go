package kyoolibs

type s3d struct {
	S3SchemaVersion string `json:"s3SchemaVersion"`
	ConfigurationId string `json:"configurationId"`
	Bucket          bucket `json:"bucket"`
	Object          object `json:"object"`
}

type responseElements struct {
	XamzRequestId string `json:"x-amz-request-id"`
	XamzId2       string `json:"x-amz-id-2"`
}

type bucket struct {
	Name          string      `json:"name"`
	OwnerIdentity interface{} `json:"ownerIdentity"`
	Arn           string      `json:"arn"`
}

type object struct {
	Key       string `json:"key"`
	Size      int    `json:"size"`
	Sequencer string `json:"sequencer"`
}

type requestParameters struct {
	SourceIPAddress string `json:"sourceIPAddress"`
}

type record struct {
	EventVersion      string            `json:"eventVersion"`
	EventSource       string            `json:"eventSource"`
	AwsRegion         string            `json:"awsRegion"`
	EventTime         string            `json:"eventTime"`
	EventName         string            `json:"EventName`
	UserIdentity      interface{}       `json:"userIdentity"`
	RequestParameters requestParameters `json:"requestParameters"`
	ResponseElements  responseElements  `json:"responseElements"`
	S3                s3d               `json:"s3"`
}

type Body struct {
	Records []record `json:"Records"`
}
