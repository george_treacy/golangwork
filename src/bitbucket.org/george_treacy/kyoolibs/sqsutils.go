package kyoolibs

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"log"
	"github.com/davecgh/go-spew/spew"
	"strconv"
	"encoding/json"
)

func NumInFlight(svc *sqs.SQS, queueUrl *string) int {
	params := &sqs.GetQueueAttributesInput{
		QueueUrl: queueUrl,
		AttributeNames: []*string{
			aws.String("ApproximateNumberOfMessagesNotVisible"),
		},
	}
	spew.Dump(params)
	resp, err := svc.GetQueueAttributes(params)

	spew.Dump(resp)

	if err != nil {
		log.Println(err.Error())
		return -1
	}
	numInFlight, err := strconv.Atoi(*resp.Attributes["ApproximateNumberOfMessagesNotVisible"])
	if err != nil {
		numInFlight = -1
	}
	return numInFlight
}

func ReceiveMessages(svc *sqs.SQS, queueUrl *string, numMessages int) []*sqs.Message {
	params := &sqs.ReceiveMessageInput{
		QueueUrl:              queueUrl,
		AttributeNames:        []*string{},
		MaxNumberOfMessages:   aws.Int64(int64(numMessages)),
		MessageAttributeNames: []*string{},
		WaitTimeSeconds:       aws.Int64(10),
	}
	resp, err := svc.ReceiveMessage(params)
	if err != nil {
		log.Println("Receive Messages", err.Error())
		return nil
	}

	if len(resp.Messages) > 0 {
		return resp.Messages
	} else {
		log.Printf("fell through after waiting......\n")
		return nil
	}
}

func ReceiveMessage(svc *sqs.SQS, queueUrl *string) *sqs.Message {
	params := &sqs.ReceiveMessageInput{
		QueueUrl:              queueUrl,
		AttributeNames:        []*string{},
		MaxNumberOfMessages:   aws.Int64(1),
		MessageAttributeNames: []*string{},
		VisibilityTimeout:     aws.Int64(1),
		WaitTimeSeconds:       aws.Int64(10),
	}
	resp, err := svc.ReceiveMessage(params)

	if err != nil {
		log.Println("Receive Message", err.Error())
		return nil
	}

	if len(resp.Messages) == 1 {
		return resp.Messages[0]
	} else if len(resp.Messages) >= 1 {
		log.Printf("more than one message, should be impossible...\n")
		return nil
	} else {
		log.Printf("fell through after waiting...\n")
		return nil
	}
}

func DeleteMessage(svc *sqs.SQS, message *sqs.Message, queueUrl *string) {
	params := &sqs.DeleteMessageInput{
		QueueUrl:      queueUrl,
		ReceiptHandle: message.ReceiptHandle,
	}
	_, err := svc.DeleteMessage(params)

	if err != nil {
		log.Println("Unknown etag - Delete Message", err.Error())
	}
}

func PushMessage(svc *sqs.SQS, summary *S3Summary, queue2Url *string) error {
	summaryJson, err := json.Marshal(summary)
	summaryString := string(summaryJson)
	//spew.Dump(summaryJson)
	if err != nil {
		return err
	}
	params := &sqs.SendMessageInput{
		QueueUrl:      	queue2Url,
		MessageBody:  	&summaryString,
	}
	_, err = svc.SendMessage(params)

	if err != nil {
		log.Println(summary.Etag, "Send Message", err.Error())
	}
	return nil
}
