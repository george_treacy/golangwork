package kyoolibs

import (
	"database/sql"
	"log"
	_ "github.com/go-sql-driver/mysql"
	"fmt"
)

/*
	returns two bools,
	one: true if etag is found
	two: true if etag is found and bucket and key match.

	if there is an etag match but no bucket, key match, then insert into dupes
	if there is an etag match and a bucket, key match then do nothing.

*/
func DupeCheckFileHeader(tx *sql.Tx, summary *S3Summary) (bool, bool) {
	rows, error := tx.Query("SELECT bucket, keey from file_header where etag=?", summary.Etag)
	checkErr(error)

	var bucket string
	var key string
	var matchEtag bool
	if rows.Next() {
		matchEtag = true
		err := rows.Scan(&bucket, &key)
		if err != nil {
			log.Fatal(summary.Etag, "DupeCheckFileHeader", err)
		}
	}
	rows.Close()
	log.Println(summary.Etag, "HeaderDupeCheck", "IncomingBucket", summary.Bucket, "IncomingKey", summary.Key, "db-bucket", bucket, "db-key", key, matchEtag)
	matchBucketKey := summary.Bucket == bucket && summary.Key == key
	return matchEtag, matchEtag && matchBucketKey
}

/*
	returns true if file_duplicate has a matching etag + path_md5
*/
func DupeCheckDuplicate(tx *sql.Tx, summary *S3Summary) bool {
	rows, error := tx.Query("SELECT count(*) from file_duplicate where etag=? and path_md5=?", summary.Etag, summary.PathMd5)
	checkErr(error)
	var count int
	if rows.Next() {
		err := rows.Scan(&count)
		if err != nil {
			log.Fatal(summary.Etag, "DupeCheckDuplicate", err)
		}
	}
	rows.Close()
	log.Println(summary.Etag, "Dupe check duplicate")
	return count != 0
}

func DoSqlWriteFileHeader(tx *sql.Tx, s3Summary *S3Summary) error {

	log.Println(s3Summary.Bucket, s3Summary.Key, s3Summary.Etag)

	stmt, err := tx.Prepare("INSERT file_header SET etag=?,bucket=?,keey=?")
	defer stmt.Close()
	checkErr(err)

	_, err = stmt.Exec(s3Summary.Etag, s3Summary.Bucket, s3Summary.Key)
	if err != nil {
		fmt.Println(s3Summary.Etag, "sql excecption", err)
		log.Println(s3Summary.Etag, "sql excecption", err)
		return err
	}
	stmt.Close()
	log.Println(s3Summary.Etag, "Wrote to file header")
	return nil
}

// todo: add error handling to this func.
func DoSqlWriteDuplicate(tx *sql.Tx, s3Summary *S3Summary) {

	stmt, err := tx.Prepare("INSERT file_duplicate SET etag=?,path_md5=?,bucket=?,keey=?")
	defer stmt.Close()
	checkErr(err)

	_, err = stmt.Exec(s3Summary.Etag, s3Summary.PathMd5, s3Summary.Bucket, s3Summary.Key)
	if err != nil {
		log.Fatal(err)
	}
	stmt.Close()
	log.Println(s3Summary.Etag, "Wrote to file duplicate")
}

func AttemptInsertDuplicate(tx *sql.Tx, s3Summary *S3Summary) {
	if !DupeCheckDuplicate(tx, s3Summary) {
		log.Println(s3Summary.Etag, "writing to duplicate table.")
		DoSqlWriteDuplicate(tx, s3Summary)
	} else {
		log.Println(s3Summary.Etag, "already have this dupe in the dupe table. do nothing.")
	}
}

/*
	It the success flag is true, we added a file to the header so we want to send a message
	to the next q.

	If the err is not nil, then we do not want to do a delete otherwise delete.
 */
func AttemptWriteFileHeader(db *sql.DB, s3Summary *S3Summary) (bool, error) {

	var success bool

	tx, err := db.Begin()
	if err != nil {
		log.Fatal(err)
	}

	dupeEtag, dupeEtagWithMatchingPath := DupeCheckFileHeader(tx, s3Summary)
	if !dupeEtag {
		log.Println(s3Summary.Etag, "writing to header table.")
		err = DoSqlWriteFileHeader(tx, s3Summary)
		if err != nil {
			return false, err
		} else {
			success = true
		}
	} else if !dupeEtagWithMatchingPath {
		AttemptInsertDuplicate(tx, s3Summary)
	} else {
		log.Println(s3Summary.Etag, "full dupe in header table. do nothing.")
	}

	err = tx.Commit()
	if err != nil {
		log.Fatal(err)
	}
	return success, nil
}

func checkErr(err error) {
	if err != nil {
		panic(err)
	}
}
