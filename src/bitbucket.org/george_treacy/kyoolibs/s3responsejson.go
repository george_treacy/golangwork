package kyoolibs

import (
	"time"
)

type Content struct {
	ETag      			string
	Key 				string
	LastModified 		time.Time
	Owner 				interface{}
	Size 				int64
	StorageClass 		string
}

type Response struct {
	Contents 			[]Content `json:"Contents"`
	IsTruncated			bool
	Marker 				string
	MaxKeys 			int
	Name 				string
	Prefix 				string
}
