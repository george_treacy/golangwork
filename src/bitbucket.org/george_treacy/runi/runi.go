// This sample program demonstrates how to use the work package
// to use a pool of goroutines to get work done.
package main

import (
	"time"
	"fmt"
	"bitbucket.org/george_treacy/kyoolibs"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/aws/aws-sdk-go/aws"
	"encoding/json"
	"bitbucket.org/george_treacy/work"
	"github.com/davecgh/go-spew/spew"
)

var command string = "ls -l"

var (
	numMessages			int		= 1
	region 				string 	= "us-west-2"
	sourceQueueUrl      *string = aws.String("https://sqs.us-west-2.amazonaws.com/911908773506/geo-queue-2")
	destinationQueueUrl *string = aws.String("https://sqs.us-west-2.amazonaws.com/911908773506/geo-queue-3")
	queryString         *string = aws.String("george:Yellowsun22@tcp(geo-001.cjkh6rtk2yht.us-west-2.rds.amazonaws.com:3306)/masso")
)

type Config struct {
	Region              string `yaml:"region"`
	SourceQueueUrl      string `yaml:"sourceQueueUrl"`
	DestinationQueueUrl string `yaml:"destinationQueueUrl"`
	QueryString         string `yaml:"queryString"`
}

// namePrinter provides special support for printing names.
type commander struct {
	command string
	args    []string
}

// Task implements the Worker interface.
func (comm *commander) Task() {
	fmt.Println(comm.command, comm.args[0])
	time.Sleep(time.Millisecond)
}

func main() {
	// Create a work pool with 2 goroutines.
	// This limits the number of files moving though
	// the tool chain at one time.
	p := work.New(2)

	// get an aws sqs service
	sqsService := sqs.New(&aws.Config{Region: aws.String(region)})

	for {
		messages := kyoolibs.ReceiveMessages(sqsService, sourceQueueUrl, numMessages)

		for _, message := range messages {
			summary := getSummaryFromMessge(message)

			commo := commander{
				command: "echo",
				args: []string{summary.Etag},
			}

			go func() {
				// Submit the task to be worked on. When RunTask
				// returns we know it is being handled.
				p.Run(&commo)
			}()

		}
	}
}

func getSummaryFromMessge(message *sqs.Message) *kyoolibs.S3Summary {
	summary := kyoolibs.S3Summary{}
	spew.Dump(message)
	err := json.Unmarshal([]byte(*message.Body), &summary)
	if err != nil {
		fmt.Println("error unmarshalling json from message.")
		panic(err)
	}

	spew.Dump("Post unmarshal", summary)

	return &summary
}

