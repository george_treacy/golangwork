package main

import (
	"bitbucket.org/george_treacy/kyoolibs"
	"database/sql"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/davecgh/go-spew/spew"
	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
	"time"
)

var (
	numMessages			int		= 10
	region              string  = "us-west-2"
	sourceQueueUrl      *string = aws.String("https://sqs.us-west-2.amazonaws.com/911908773506/geo-queue-1")
	destinationQueueUrl *string = aws.String("https://sqs.us-west-2.amazonaws.com/911908773506/geo-queue-2")
	queryString         *string = aws.String("george:Yellowsun22@tcp(geo-001.cjkh6rtk2yht.us-west-2.rds.amazonaws.com:3306)/masso")
)

type Config struct {
	Region              string `yaml:"region"`
	SourceQueueUrl      string `yaml:"sourceQueueUrl"`
	DestinationQueueUrl string `yaml:"destinationQueueUrl"`
	QueryString         string `yaml:"queryString"`
}

//quereyString string = "george:Bluemoon22@tcp(geo-db-oregon.ctsaruyackoy.us-west-2.rds.amazonaws.com:3306)/geo_redeye"

func init() {
	loadConfig()
}

func main() {

	// initialise logging
	f, err := os.OpenFile("testlogfile", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	defer f.Close()
	log.SetOutput(f)
	log.Println("Kyoo has started...")

	// get an aws sqs service
	svc := sqs.New(&aws.Config{Region: aws.String(region)})

	// initialize mysql
	db, err := sql.Open("mysql", *queryString)
	defer db.Close()
	db.SetMaxIdleConns(10)
	db.SetMaxOpenConns(50)

	if err != nil {
		log.Printf("Can't open sql connection: %s", err.Error())
		panic(err)
	}

	var messageCount int

	var numInFlight int

	go func() {
		for {
			// get the number of messages in flight once per interval.
			// default 10 seconds.
			time.Sleep(10 * time.Second)
			numInFlight = kyoolibs.NumInFlight(svc, sourceQueueUrl)
		}
	}()

	for {
		for numInFlight > 500 {
			time.Sleep(5 * time.Second)
		}
		messages := kyoolibs.ReceiveMessages(svc, sourceQueueUrl)
		for _, message := range messages {

			messageCount++
			log.Println(messageCount, "***********************************************************")

			s3Service := s3.New(&aws.Config{Region: aws.String(region)})
			s3Summary := kyoolibs.HeadObject(s3Service, message)
			log.Println(s3Summary.Etag, messageCount, "got summary from s3: ", s3Summary)

			doDelete := make(chan bool)
			pushQueue := make(chan bool)

			if s3Summary == nil {
				kyoolibs.DeleteMessage(svc, message, sourceQueueUrl)
			} else {
				go attemptWriteSql(doDelete, pushQueue, s3Summary, db)

				go deleteMessage(doDelete, pushQueue, svc, sourceQueueUrl, message, s3Summary)

				go pushMessage(pushQueue, svc, s3Summary)
			}
		}
	}
}

// load config file
func loadConfig() {
	dat, err := ioutil.ReadFile("properties")
	var config Config
	err = yaml.Unmarshal(dat, &config)
	if err != nil {
		fmt.Println("wtf", err.Error())
		panic("Can't load config file.")
	} else {
		sourceQueueUrl = &config.SourceQueueUrl
		destinationQueueUrl = &config.DestinationQueueUrl
		queryString = &config.QueryString
		region = config.Region
	}
	spew.Dump(config)
}

func attemptWriteSql(doDelete chan bool, pushQueue chan bool, s3Summary *kyoolibs.S3Summary, db *sql.DB) {
	success, err := kyoolibs.AttemptWriteFileHeader(db, s3Summary)
	pushQueue <- success // todo: this signal will fire about the same time as the delete, not after the delete, is this a problem?
	if err != nil {
		doDelete <- false // we will try again later due to contention with mysql.
	} else {
		doDelete <- true
	}
}

/* 	just sqs delete to first q.
todo: can these become zombies if no doDelete message?
no. So long s the Mysql Proc completes, doDelete will be true
and this proc will run and complete.
*/
func deleteMessage(doDelete chan bool, pushQueue chan bool, svc *sqs.SQS, queueUrl *string, message *sqs.Message, s3Summary *kyoolibs.S3Summary) {
	if <-doDelete {
		log.Println(s3Summary.Etag, "I will do the delete")
		kyoolibs.DeleteMessage(svc, message, sourceQueueUrl)
	}
}

func pushMessage(pushQueue chan bool, svc *sqs.SQS, s3Summary *kyoolibs.S3Summary) {
	if <-pushQueue {
		log.Println(s3Summary.Etag, "I will push to new q.")
		kyoolibs.PushMessage(svc, s3Summary, destinationQueueUrl)
	} else {
		// no header added so we don't add this one to the queue for further processing.
	}
}
